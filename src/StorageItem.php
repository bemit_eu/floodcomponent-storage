<?php

namespace Flood\Component\Storage;

abstract class StorageItem {
    protected function parse($data, $convert_json = [], $convert_lower = [], $convert_upper = []) {
        if(is_array($data)) {
            foreach($data as $key => $item) {
                if(in_array(strtolower($key), $convert_lower)) {
                    $key = strtolower($key);
                } else if(in_array(strtolower($key), $convert_upper)) {
                    $key = strtoupper($key);
                }
                if(property_exists($this, $key)) {
                    if(in_array(strtolower($key), $convert_json) && $item) {
                        $this->$key = json_decode($item, true);
                    } else {
                        $this->$key = $item;
                    }
                }
            }
        }
    }
}