<?php

namespace Flood\Component\Storage;

use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Types\JsonType;
use Doctrine\DBAL\DriverManager;
use Flood\Component\Sonar\Annotation\Service;

/**
 *
 * @Service(id="storage-data", executed=false)
 * @package Flood\Component\Storage
 */
class Storage {
    /**
     * @var callable
     */
    public static $config;

    /**
     * @var array
     */
    protected $connection_list = [];

    /**
     * @var int|string the default db used
     */
    public const DEFAULT_DB = 0;

    /**
     * @param null|mixed $id the id of the wanted database, implements fallback to static::DEFAULT_DB
     *
     * @return \Doctrine\DBAL\Connection
     * @throws
     *
     */
    public function getDatabase($id = null) {
        if(!$this->existConnection($id)) {
            $this->connectDb($id);
        }

        if($this->existConnection($id)) {
            return $this->getConnection($id);
        }

        throw new \Exception('FloodComponent\Storage: database not found for id `' . $id . '`');
    }

    /**
     * @param string $sql
     * @param null|mixed $id the id of the wanted database, implements fallback to static::DEFAULT_DB
     *
     * @return \Doctrine\DBAL\Driver\Statement
     */
    public function prepare($sql, $id = null) {
        try {
            return $this->getDatabase($id)->prepare($sql);
        } catch(DBALException $e) {
            error_log($e->getMessage());

            return null;
        }
    }

    /**
     * @param array $column
     * @param       $tables
     * @param       $condition
     * @param       $debug
     *
     * @return \Doctrine\DBAL\Driver\Statement
     */
    public function select($column, $tables, $condition = null, $debug = false) {
        $joins = false;
        if(isset($condition['JOIN'])) {
            $joins = $condition['JOIN'];
            unset($condition['JOIN']);
        }
        $groups = false;
        if(isset($condition['GROUP'])) {
            $groups = $condition['GROUP'];
            unset($condition['GROUP']);
        }
        $limit = false;
        if(isset($condition['LIMIT'])) {
            $limit = $condition['LIMIT'];
            unset($condition['LIMIT']);
        }
        /*$order = false;
        if(isset($condition['ORDER'])) {
            $order = $condition['ORDER'];
            unset($condition['ORDER']);
        }*/
        // todo: add also "ORDER BY" here, is below in `buildCondition` currently
        $select = 'SELECT ' . $this->columnConcat($column) .
            ' FROM ' . $this->tableConcat($tables) .
            ($joins ? ' ' . $this->buildJoin($joins) . ' ' : '') .
            (isset($condition) && !empty($condition) ? ' WHERE ' . $this->buildCondition($condition) : '') .
            ($groups ? ' GROUP BY ' . $this->columnConcat($groups) : '') .
            //($order ? ' ORDER BY ' . $order : '') .
            ($limit ? ' LIMIT ' . $limit : '');
        if($debug) {
            error_log($select);
        }
        return $this->prepare($select);
    }

    /**
     * @param string $tableExpression
     * @param array $identifier
     * @param array $types
     * @param null|mixed $id the id of the wanted database, implements fallback to static::DEFAULT_DB
     *
     * @return integer The number of affected rows.
     */
    public function delete($tableExpression, array $identifier, array $types = [], $id = null) {
        try {
            return $this->getDatabase($id)->delete($tableExpression, $identifier, $types);
        } catch(DBALException $e) {
            echo $e->getMessage() . "\r\n";

            return null;
        }
    }

    /**
     * @param string $tableExpression
     * @param array $data
     * @param array $types
     * @param bool|string $auto_id when true hydro will generate a new ID with a hex string on column `ID`
     * @param null|mixed $db_id the id of the wanted database, implements fallback to static::DEFAULT_DB
     *
     * @return integer|null|string The number of affected rows, null when error, the id when auto_id is on
     * @todo: find a way to catch errors more specific and not just some "anywhere in the dbal has happend some error"
     *
     */
    public function insert($tableExpression, array $data, array $types = [], $auto_id = false, $db_id = null) {
        try {
            if($auto_id) {
                if(is_string($auto_id)) {
                    $data[$auto_id] = $this->randomId();
                } else {
                    $data['ID'] = $this->randomId();
                }
            }

            $inserted = $this->getDatabase($db_id)->insert($tableExpression, $this->columnsEscape($data), $types);

            if($auto_id) {
                return $data['ID'];
            } else {
                return $inserted;
            }
        } catch(DBALException $e) {

            error_log($e->getMessage());

            return null;
        }
    }

    /**
     * @param string $tableExpression
     * @param array $data
     * @param array $identifier
     * @param array $types
     * @param null|mixed $id the id of the wanted database, implements fallback to static::DEFAULT_DB
     *
     * @return integer|bool The number of affected rows.
     */
    public function update($tableExpression, array $data, array $identifier, array $types = [], $id = null) {
        if(is_array($data)) {
            $data = $this->columnsEscape($data);
        }

        if(empty($data)) {
            error_log('FloodComponent\Storage: Error, tried to update table `' . $tableExpression . '` with empty data');

            return false;
        }

        try {
            return $this->getDatabase($id)->update($tableExpression, $data, $identifier, $types);
        } catch(DBALException $e) {
            error_log($e->getMessage());

            return false;
        }
    }

    /**
     * Generates a random id with a fixed size, uses bloom-data to have a much less collision possibility.
     * As sha1 could be truncated as the unpredictability of any part of the hash is proportional to the part's size (for non-cryptographic uses).
     * Performance was improved as much as possible with using str functions and not rand but in a way to ensure less collisions.
     *
     * @return bool|string
     * @todo remove fixed length and implement per-db
     *
     */
    public function randomId() {
        $time = microtime(true);

        return substr(hash('sha1', strrev($time) . 'aAbB' . $time . str_shuffle('aBcDe+Fg=hi#Ijk@L$mnOPqrs') . strrev($time) . 'xXyYzZ' . str_shuffle($time)), 0, 20);
    }

    /**
     * @param array $column [ 'column_1', '`column-2`' ] - need to be escaped/surrounded when needed
     *
     * @return bool|string
     */
    public function columnConcat($column) {
        foreach($column as &$col) {
            $col = $this->columnEscape($col);
        }

        return implode(', ', $column);
    }

    /**
     * @param array $columns column containing data array like ['col_1' => 'some-data',]
     *
     * @return array
     */
    public function columnsEscape($columns) {
        // todo: refine escaping of column names
        $new_data = [];
        foreach($columns as $key => $dat) {
            $new_data[$this->columnEscape($key)] = $dat;
        }

        return $new_data;
    }

    /**
     * Escape one column with these ticks ` - but only if it doesn't have them on their own
     * Removes comment in column name if exists, comment is used to be able to use nested conditions
     *
     * @param $column
     *
     * @return string
     * @todo refine
     *
     */
    public function columnEscape($column) {
        $column = $this->removeComment($column);

        if(false !== strpos($column, '(')) {
            // when the column contains a function, don't escape
            // todo: escape columns inside the function
            return $column;
        }

        if(false !== strpos($column, '.')) {
            // when the column is actually something with like table_name.column_name
            $columns = explode('.', $column);
            $cols = [];
            foreach($columns as $col) {
                $cols[] = $this->columnEscape($col);
            }

            return implode('.', $cols);
        }

        if(0 !== strpos($column, '`')) {
            $column = '`' . $column;
        }

        if(0 !== strpos(strrev($column), '`')) {
            $column = $column . '`';
        }

        return $column;
    }

    public function tableConcat($tables) {
        if(!is_array($tables)) {
            $tables = [$tables];
        }

        $table_expr = [];

        foreach($tables as $table) {
            $t = trim($table);
            if(!substr($t, 0, 1) === '`') {
                $t = '`' . $t;
            }
            if(!substr(strrev($t), 0, 1) === '`') {
                $t .= '`';
            }
            $table_expr[] = $t;
        }

        return implode(', ', $table_expr);
    }

    public function removeComment($subject) {
        if(false !== strpos($subject, '#')) {
            $subject = substr($subject, 0, strpos($subject, '#'));
        }

        return $subject;
    }

    public function buildJoin($joins) {
        $str_join = '';
        foreach($joins as $table => $on) {
            if(0 === strpos($table, '<')) {
                $str_join .= ' LEFT';
            } else if(0 === strpos($table, '>')) {
                $str_join .= ' RIGHT';
            }
            $str_join .= ' JOIN ' . $this->columnEscape(substr($table, 1)) . ' ON ';
            foreach($on as $left => $right) {
                $str_join .= $this->columnEscape($left) . '=' . $this->columnEscape($right);
            }
        }
        return $str_join;
    }

    /**
     * @param $condition
     * @param $combine
     *
     * @return string
     * @todo implement that it will build correctly for more then one index
     *
     */
    public function buildCondition($condition, $combine = 'AND') {
        $condition_str = '';
        $i = 0;
        $qty = count($condition);
        foreach($condition as $key => $value) {
            // $key: could be column name or something like OR, AND, ORDER
            if(true === $value) {
                $value = 1;
            } else if(false === $value) {
                $value = 0;
            }
            if(is_array($value)) {
                // nested condition found, $key must be one of: OR, AND, ORDER
                $key = $this->removeComment($key);
                switch($key) {
                    case 'OR':
                    case 'AND':
                        // only prepend OR/AND when not the first column
                        $condition_str .= ' ' . (0 < $i && $i < $qty ? $combine : '') . ' (' . $this->buildCondition($value, $key) . ')';
                        break;
                    case 'ORDER':
                        $condition_str .= ' ORDER BY ';
                        // todo: also a minimum of one OR/AND is needed as it would be WHERE ORDER BY instead of only ORDER BY
                        // todo: only one is possible atm
                        foreach($value as $col => $order) {
                            $condition_str .= $col . ' ' . $order;
                        }
                        break;
                }
            } else {
                if(0 < $i && $i < $qty) {
                    // only prepend the AND when not the first column
                    $condition_str .= ' ' . $combine . ' ';
                }
                $i++;
                if(is_string($value)) {
                    if(0 === strpos($value, '!')) {
                        // build NOT
                        // todo: add `not like`
                        if(strtolower(substr($value, 1)) === 'null') {
                            $condition_str .= $this->columnEscape($key) . ' NOT NULL';
                        } else {
                            $condition_str .= $this->columnEscape($key) . ' NOT "' . substr($value, 1) . '"';
                        }
                    } else if(0 === strpos($value, '~')) {
                        // build LIKE
                        $condition_str .= $this->columnEscape($key) . ' LIKE "' . substr($value, 1) . '"';
                    } else if(0 === strpos($value, '=')) {
                        // build FULL INNER JOIN
                        $condition_str .= $this->columnEscape($key) . ' = ' . $this->columnEscape(substr($value, 1));
                    } else if(0 === strpos($value, '>')) {
                        // build bigger then
                        $condition_str .= $this->columnEscape($key) . ' > "' . substr($value, 1) . '"';
                    } else if(0 === strpos($value, '<')) {
                        // build smaller then
                        $condition_str .= $this->columnEscape($key) . ' < "' . substr($value, 1) . '"';
                    } else if(0 === strpos($value, '>=')) {
                        // build bigger then
                        $condition_str .= $this->columnEscape($key) . ' >= "' . substr($value, 2) . '"';
                    } else if(0 === strpos($value, '<=')) {
                        // build smaller then
                        $condition_str .= $this->columnEscape($key) . ' <= "' . substr($value, 2) . '"';
                    } else {
                        // build IS
                        $condition_str .= $this->columnEscape($key) . ' = "' . $value . '"';
                    }
                } else {
                    if($value === null) {
                        $condition_str .= $this->columnEscape($key) . ' is null';
                    } else {
                        $condition_str .= $this->columnEscape($key) . ' = ' . $value . '';
                    }
                }
            }
        }

        return $condition_str;
    }

    /**
     * Connects to a db using the configuration for that $id
     *
     * @param null|mixed $id the id of the wanted database, implements fallback to static::DEFAULT_DB
     */
    public function connectDb($id = null) {
        if(null === $id) {
            $id = static::DEFAULT_DB;
        }

        try {
            $config = static::$config;
            if(is_array($config(['server', $id]))) {
                try {
                    $this->setConnection(
                        $id,
                        DriverManager::getConnection(
                            [
                                'dbname' => $config(['server', $id, 'name']),
                                'user' => $config(['server', $id, 'user']),
                                'password' => $config(['server', $id, 'pass']),
                                'host' => $config(['server', $id, 'server']),
                                'driver' => $config(['server', $id, 'driver']),
                                'charset' => $config(['server', $id, 'charset']),
                            ],
                            new Configuration()
                        )
                    );
                } catch(DBALException $e) {
                    error_log($e->getMessage());
                }
            } else {
                // when the wanted db doesn't exist in configuration
                throw new \Exception('DB Error: wanted db is not configured: ' . $id);
            }
        } catch(\Exception $e) {
            error_log($e->getMessage());
        }

        //error_log(json_encode(Type::getType('json')));
        //Type::addType('json', 'Doctrine\DBAL\Types\JsonType');
    }

    /**
     * Retrieves an established db connection to memory
     *
     * @param null|mixed $id the id of the database that will be set
     * @param            $connection
     */
    protected function setConnection($id, $connection) {
        $this->connection_list[$id] = $connection;
    }

    /**
     * Retrieves an already established connection to a db
     *
     * @param null|mixed $id the id of the wanted database, implements fallback to static::DEFAULT_DB
     *
     * @return \Doctrine\DBAL\Connection
     */
    protected function getConnection($id = null) {
        if(null === $id) {
            $id = static::DEFAULT_DB;
        }

        return $this->connection_list[$id];
    }

    /**
     * Check if an db connection is already registered
     *
     * @param null|mixed $id the id of the wanted database, implements fallback to static::DEFAULT_DB
     *
     * @return bool
     */
    protected function existConnection($id = null) {
        if(null === $id) {
            $id = static::DEFAULT_DB;
        }

        return (isset($this->connection_list[$id]) && is_object($this->connection_list[$id]));
    }
}
