# Flood\Hydro\Storage

Handling connection to database storages.

## Data Select Syntax

```php
<?php

Hydro\Container::_storageData()
    ->select(
        ['user.ID', 'user.name', 'group.name'],// columns 
        ['user', 'group'],// tables
        [// condition/where
            'active' => true,// `active` = 1
            'zip' => '~75%',// `zip` LIKE "75%"
            'group.name' => '!demo',// `group.name` NOT "demo",
            'OR' => [
                'group.name' => 'customer',// `group.name` = "customer",
                'group.name#2' => 'free',// `group.name` = "free",
            ],
            'group.team' => '=user.group',// `group` NOT "demo"
            'ORDER' => ['zip' => 'ASC'],
        ],
        true // debug
    );

/*
 * SELECT
 *     `user`.`ID`, `user`.`name`, `group`.`name`
 * FROM
 *     `user`, `group`
 * WHERE
 *     `active` = 1 AND
 *     `zip` LIKE "75%" AND
 *     `group.name` NOT "demo" AND
 *     (
 *         `group.name` = "customer" OR
 *         `group.name` = "free"
 *     ) AND
 *     `group`.`team` = `user`.`group`
 * ORDER BY `zip` ASC  
 */
```